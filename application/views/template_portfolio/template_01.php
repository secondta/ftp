<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
    <style>
    .no-scrollbar::-webkit-scrollbar {
        display: none;
    }

    .no-scrollbar {
        -ms-overflow-style: none;
        scrollbar-width: none;
    }
    </style>
</head>

<body class="no-scrollbar">
    <div class="all font-web">
        <div class="creator bg-[#dedede] px-[100px] py-12">
            <div class="grid grid-cols-2 gap-10">
                <div class="flex items-center" data-aos="fade-right" data-aos-duration="2000">
                    <div>
                        <p class="text-5xl font-bold">
                            Hi There,
                        </p>
                        <p class="text-5xl font-bold mt-3">
                            I'm <span class="text-cyan-600">John Doe</span>
                        </p>
                        <p class="text-2xl font-bold mt-3">
                            I Am Into <span class="text-cyan-600"> Frontend Developer</span>
                        </p>
                        <p class="text-sm text-justify mt-3">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis porro, consequatur officiis
                            quae magni reiciendis fuga doloremque accusamus. Omnis repellendus id, veniam corrupti non
                            velit nobis unde aut, pariatur quos eos sed architecto modi illum enim? Modi eligendi enim
                            ea earum consequatur illo commodi dolores praesentium, quo nulla? Odio obcaecati.
                        </p>
                        <div class="icon flex gap-5">
                            <i
                                class="fa-brands fa-instagram mt-3 cursor-pointer text-xl bg-white black text-black shadow-lg w-[40px] h-[40px] flex justify-center items-center rounded-full"></i>
                            <i
                                class="fa-brands fa-linkedin-in mt-3 cursor-pointer text-xl bg-white black text-black shadow-lg w-[40px] h-[40px] flex justify-center items-center rounded-full"></i>
                            <i
                                class="fa-brands fa-telegram mt-3 cursor-pointer text-xl bg-white black text-black shadow-lg w-[40px] h-[40px] flex justify-center items-center rounded-full"></i>
                            <i
                                class="fa-brands fa-x-twitter mt-3 cursor-pointer text-xl bg-white black text-black shadow-lg w-[40px] h-[40px] flex justify-center items-center rounded-full"></i>
                        </div>
                    </div>
                </div>
                <div class="flex justify-center items-center" data-aos="fade-left" data-aos-duration="2000">
                    <img class="w-[55%] mx-auto mt-6 rounded-full"
                        src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" alt="Creator 01">
                </div>
            </div>
        </div>
        <div class="about-me bg-[#d4d4d4] px-[100px] py-12">
            <div class="grid grid-cols-2 gap-10">
                <div class="flex justify-center items-center" data-aos="fade-left" data-aos-duration="2000">
                    <img class="w-[55%] mx-auto mt-6 rounded-lg"
                        src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" alt="Creator 01">
                </div>
                <div class="flex items-center" data-aos="fade-right" data-aos-duration="2000">
                    <div>
                        <p class="text-5xl font-bold">
                            About Me
                        </p>
                        <div class="text-md mt-8 px-[50px]">
                            <ul class="list-disc">
                                <li>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime ratione ut iste, ad
                                    harum sunt tempore molestias quos voluptatem magnam.
                                </li>
                                <li class="mt-5 text-sm">
                                    <span class="font-bold">My Skills Are:</span> Lorem ipsum dolor, sit amet
                                    consectetur adipisicing elit. Molestiae, vero!
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="service bg-[#dedede] px-[100px] py-12">
            <p class="text-5xl font-bold text-center">
                Services.
            </p>
            <div class="grid grid-cols-3 gap-10 mt-12">
                <div class="card-01 flex justify-center" data-aos="fade-down" data-aos-duration="1500">
                    <div class="card border-2 z-40 border-black flex items-center h-[320px] w-[70%] px-5">
                        <div>
                            <p class="icon-card text-3xl">
                                <i class="fa-solid fa-circle-dot"></i>
                            </p>
                            <p class="judul-card text-xl font-bold">
                                Service 01
                            </p>
                            <p class="judul-card text-sm mt-5">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Blanditiis dicta vitae, ab
                                inventore repellendus alias magni! Dicta, qui? Cumque, accusantium.
                            </p>
                        </div>
                    </div>
                    <!-- <div class="card border-2 z-0 border-black absolute h-[320px] w-[18.5%] mt-5 ml-9 px-5">
                    </div> -->
                </div>
                <div class="card-02 flex justify-center" data-aos="fade-down" data-aos-duration="1500"
                    data-aos-delay="200">
                    <div class="card border-2 border-black flex items-center h-[320px] w-[70%] px-5">
                        <div>
                            <p class="icon-card text-3xl">
                                <i class="fa-solid fa-circle-dot"></i>
                            </p>
                            <p class="judul-card text-xl font-bold">
                                Service 02
                            </p>
                            <p class="judul-card text-sm mt-5">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Blanditiis dicta vitae, ab
                                inventore repellendus alias magni! Dicta, qui? Cumque, accusantium.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card-03 flex justify-center" data-aos="fade-down" data-aos-duration="1500"
                    data-aos-delay="400">
                    <div class="card border-2 border-black flex items-center h-[320px] w-[70%] px-5">
                        <div>
                            <p class="icon-card text-3xl">
                                <i class="fa-solid fa-circle-dot"></i>
                            </p>
                            <p class="judul-card text-xl font-bold">
                                Service 03
                            </p>
                            <p class="judul-card text-sm mt-5">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Blanditiis dicta vitae, ab
                                inventore repellendus alias magni! Dicta, qui? Cumque, accusantium.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project bg-[#d4d4d4] px-[100px] py-12">
            <p class="text-5xl font-bold text-center">
                Projects.
            </p>
            <div class="grid grid-cols-3 gap-10 mt-12">
                <div class="card-01 flex justify-center">
                    <div class="h-[100%] w-[80%] px-5" data-aos="fade-down" data-aos-duration="1500">
                        <img class="w-[100%]" src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>"
                            alt="Project Image">
                        <div class="mt-5">
                            <div class="grid grid-cols-12 gap-2 text-sm font-bold">
                                <div class="col-span-2 border-b-2 border-black mb-[9px]"></div>
                                <div class="col-span-10">Website</div>
                            </div>
                            <p class="text-lg font-bold mt-3">
                                Lorem App
                            </p>
                            <p class="text-sm mt-3">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Blanditiis dicta vitae, ab
                                inventore repellendus alias magni! Dicta, qui? Cumque, accusantium.
                            </p>
                            <div class="flex items-center gap-2 text-sm mt-3">
                                <i class="fa-solid fa-earth-asia"></i>
                                <p>Views</p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card border-2 z-0 border-black absolute h-[320px] w-[18.5%] mt-5 ml-9 px-5">
                    </div> -->
                </div>
                <div class="card-02 flex justify-center">
                    <div class="h-[100%] w-[80%] px-5" data-aos="fade-down" data-aos-duration="1500"
                        data-aos-delay="200">
                        <img class="w-[100%]" src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>"
                            alt="Project Image">
                        <div class="mt-5">
                            <div class="grid grid-cols-12 gap-2 text-sm font-bold">
                                <div class="col-span-2 border-b-2 border-black mb-[9px]"></div>
                                <div class="col-span-10">App</div>
                            </div>
                            <p class="text-lg font-bold mt-3">
                                Lorem App
                            </p>
                            <p class="text-sm mt-3">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Blanditiis dicta vitae, ab
                                inventore repellendus alias magni! Dicta, qui? Cumque, accusantium.
                            </p>
                            <div class="flex items-center gap-2 text-sm mt-3">
                                <i class="fa-solid fa-earth-asia"></i>
                                <p>Views</p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card border-2 z-0 border-black absolute h-[320px] w-[18.5%] mt-5 ml-9 px-5">
                    </div> -->
                </div>
                <div class="card-03 flex justify-center">
                    <div class="h-[100%] w-[80%] px-5" data-aos="fade-down" data-aos-duration="1500"
                        data-aos-delay="400">
                        <img class="w-[100%]" src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>"
                            alt="Project Image">
                        <div class="mt-5">
                            <div class="grid grid-cols-12 gap-2 text-sm font-bold">
                                <div class="col-span-2 border-b-2 border-black mb-[9px]"></div>
                                <div class="col-span-10">Website</div>
                            </div>
                            <p class="text-lg font-bold mt-3">
                                Lorem App
                            </p>
                            <p class="text-sm mt-3">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Blanditiis dicta vitae, ab
                                inventore repellendus alias magni! Dicta, qui? Cumque, accusantium.
                            </p>
                            <div class="flex items-center gap-2 text-sm mt-3">
                                <i class="fa-solid fa-earth-asia"></i>
                                <p>Views</p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card border-2 z-0 border-black absolute h-[320px] w-[18.5%] mt-5 ml-9 px-5">
                    </div> -->
                </div>
            </div>
        </div>
        <div class="footer bg-[#e7e7e7] grid grid-cols-3 gap-[70px] px-[100px] py-[40px]">
            <div class="footer-01">
                <p class="judul-address text-2xl font-bold">
                    Address
                </p>
                <p class="mt-6 text-justify">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eligendi quia mollitia velit esse eum
                    corporis.
                </p>
            </div>
            <div class="footer-02">
                <p class="judul-contact text-2xl font-bold">
                    Social Media
                </p>
                <p class="mt-5">johndoe@gmail.com</p>
                <p class="mt-1">+62 839-9999-9999</p>
                <p class="mt-1 flex">
                    <i class="fa-brands fa-instagram text-xl mr-3"></i>
                    <i class="fa-brands fa-github text-xl mx-3"></i>
                    <i class="fa-brands fa-gitlab text-xl mx-3"></i>
                </p>
            </div>
            <div class="footer-03">
                <form action="<?php echo base_url('user/send_email_port01') ?>" method="post">
                    <p class="judul-contact text-2xl font-bold">
                        Get Template
                    </p>
                    <p class="mt-5">
                        <input
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            name="email" id="email" type="email" placeholder="Enter Your Email Here!">
                        <button type="submit"
                            class="mt-3 inline-flex items-center justify-center h-9 px-6 font-medium tracking-wide text-white transition duration-200 bg-gray-900 rounded-lg hover:bg-gray-800 focus:shadow-outline focus:outline-none">
                            Send
                        </button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <script>
    AOS.init();
    </script>
</body>

</html>