<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
    <style>
    .no-scrollbar::-webkit-scrollbar {
        display: none;
    }

    .no-scrollbar {
        -ms-overflow-style: none;
        scrollbar-width: none;
    }

    .navbar {
        position: fixed;
        top: 0;
        width: 100%;
        padding: 15px;
        z-index: 1000;
    }

    .navbar a {
        margin: 0 10px;
        text-decoration: none;
    }

    .navbar-profile button {
        background: none;
        border: none;
        cursor: pointer;
    }

    .navbar-profile {
        display: flex;
        align-items: center;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
        right: 0;
        border-radius: 15px 0px 15px 0px;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        border-radius: 15px 0px 15px 0px;
        margin: 0;
    }

    .dropdown-content a:hover {
        background-color: #f1f1f1;
        border-radius: 15px 0px 15px 0px;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }
    </style>
</head>

<body class="no-scrollbar">
    <div class="navbar fixed w-full top-0 px-4 py-1">
        <div class="flex justify-between">
            <a href="<?php echo base_url();?>" class="w-[50px] h-[50px]">
                <img src="<?php echo base_url('uploads/logo/1704420625245-thumbnail-removebg-preview.png');?>"
                    alt="Logo" width="50px">
            </a>
            <?php if ( $this->session->userdata('logged_in') === true ) : ?>
            <div class="navbar-profile">
                <div class="dropdown iconProfile">
                    <button id="profile-dropdown" class="profile-button">
                        <i class="far fa-user text-xl"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo base_url('user/profile');?>">Profile</a>
                        <a href="<?php echo base_url('auth/logout');?>">Logout</a>
                    </div>
                </div>
            </div>
            <?php else : ?>
            <a href="<?php echo base_url('auth/login');?>"
                class="iconProfile text-lg inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 whitespace-no-wrap bg-transparent">
                Login
            </a>
            <?php endif; ?>
        </div>
    </div>

    <div class="all font-web bg-slate-100">
        <div class="welcome-page min-h-screen grid grid-cols-2">
            <div class="flex justify-center items-center bg-[#1e1e1e]">
                <img src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" alt="Logo" width="400px">
            </div>
            <div class="flex px-[110px] items-center bg-[#e7e7e7]">
                <div>
                    <div class="text-welcome">
                        <p class="text-4xl font-bold">Welcome <?php echo $this->session->userdata('username') ?></p>
                    </div>
                    <div class="web-decription mt-7 text-justify">
                        <p>
                            The website provides free and stylish portfolio templates for easy download. Users can
                            quickly customize these responsive designs for personal or professional portfolios at no
                            cost. It's a convenient resource for those looking to build or update their portfolios
                            efficiently.
                        </p>
                    </div>
                    <div class="web-contact mt-7">
                        <p>secondtaardiansyahw@gmail.com</p>
                        <p class="mt-1">+62 856-0038-1348</p>
                    </div>
                    <?php if ( $this->session->userdata('logged_in') === true ) : ?>
                    <div class="profile-button mt-7">
                        <a href="<?php echo base_url('user/profile');?>"
                            class="inline-flex items-center justify-center h-12 px-[50px] font-medium tracking-wide text-white transition duration-200 bg-gray-900 rounded-lg hover:bg-gray-850 focus:shadow-outline focus:outline-none">
                            My Profile
                        </a>
                    </div>
                    <?php else : ?>
                    <div class="login-button mt-7">
                        <a href="<?php echo base_url('auth/login');?>"
                            class="inline-flex items-center justify-center h-12 px-[50px] font-medium tracking-wide text-white transition duration-200 bg-gray-900 rounded-lg hover:bg-gray-850 focus:shadow-outline focus:outline-none">
                            Login
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="template-porto mt-[50px] px-[50px]">
            <p class="judul-1 text-4xl font-bold px-[50px]">
                Template
            </p>
            <div class="grid grid-cols-3 gap-5 mt-8">
                <?php $no = 0; foreach ($template_portfolio as $row): $no++; ?>
                <div class="card w-[85%] pl-[50px] flex justify-center">
                    <a href="<?php echo base_url('template/' . $row->link_template);?>"
                        class="max-w-sm rounded overflow-hidden shadow-lg bg-slate-50">
                        <img class="w-[80%] mx-auto mt-6 rounded-lg"
                            src="<?php echo base_url('uploads/template/' . $row->thumbnail);?>" alt="Template">
                        <div class="px-6 py-4">
                            <div class="font-bold text-xl mb-2"><?php echo $row->title; ?></div>
                            <p class="text-gray-700 text-base">
                                <?php echo $row->description; ?>
                            </p>
                        </div>
                        <div class="px-6 pt-4 pb-2">
                            <span
                                class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#<?php echo $row->tag; ?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="creator mb-[30px] mt-[50px] ml-[50px] px-[50px]">
            <p class="judul-1 text-4xl font-bold ">
                Creator
            </p>
            <div class="grid grid-cols-2 gap-10">
                <div class="flex items-center">
                    <div>
                        <p class="text-4xl font-bold">
                            Secondta Ardiansyah,
                        </p>
                        <p class="text-4xl font-bold">
                            <span class="text-cyan-600">Junior</span> Developer
                        </p>
                        <p class="text-sm text-justify mt-3">
                            Highly motivated and skilled junior programmer currently enrolled at SMK Bina Nusantara
                            Semarang, actively pursuing web development expertise through the Binus Development
                            Bootcamp. With a keen focus on creating web-based applications, I bring a strong foundation
                            in programming languages, web development technologies, and a problem-solving mindset.
                        </p>
                        <a href="<?php echo base_url('');?>"
                            class="inline-flex items-center mt-3 justify-center h-12 px-[50px] font-medium tracking-wide text-white transition duration-200 bg-gray-900 rounded-lg hover:bg-gray-850 focus:shadow-outline focus:outline-none">
                            Support
                        </a>
                    </div>
                </div>
                <div class="flex justify-center items-center">
                    <img class="w-[55%] mx-auto mt-6 rounded-lg"
                        src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" alt="Creator 01">
                </div>
            </div>
        </div>
        <div class="tutorial mb-[30px] mt-[50px] ml-[50px] px-[50px]">
            <p class="judul-1 text-4xl font-bold ">
                How To
            </p>
            <div class="grid grid-cols-12 gap-10 mt-8">
                <!-- Step 1 -->
                <div class="col-span-3 flex justify-center items-center">
                    <i class="fa-regular fa-envelope text-[75px]"></i>
                </div>
                <div class="col-span-9">
                    <div class="step text-sm text-cyan-600">STEP 1</div>
                    <div class="text-xl font-semibold">SEND EMAIL</div>
                    <div class="description mt-3">
                        Send your email in the footer and then wait until you receive your email
                    </div>
                </div>
                <div class="col-span-12">
                    <hr>
                </div>
                <!-- Step 2 -->
                <div class="col-span-3 flex justify-center items-center">
                    <i class="fa-regular fa-envelope-open text-[75px]"></i>
                </div>
                <div class="col-span-9">
                    <div class="step text-sm text-cyan-600">STEP 2</div>
                    <div class="text-xl font-semibold">OPEN EMAIL</div>
                    <div class="description mt-3">
                        Open your email then click on the link provided
                    </div>
                </div>
                <div class="col-span-12">
                    <hr>
                </div>
                <!-- Step 3 -->
                <div class="col-span-3 flex justify-center items-center">
                    <i class="fa-regular fa-pen-to-square text-[75px]"></i>
                </div>
                <div class="col-span-9">
                    <div class="step text-sm text-cyan-600">STEP 3</div>
                    <div class="text-xl font-semibold">ENTER THE REQUIRED DATA</div>
                    <div class="description mt-3">
                        After opening the link provided, you enter the required data
                    </div>
                </div>
            </div>
        </div>
        <div class="footer bg-[#e7e7e7] grid grid-cols-3 gap-[70px] px-[100px] py-[40px]">
            <div class="footer-01">
                <p class="judul-contact text-2xl font-bold">
                    Contact
                </p>
                <p class="mt-5">FTP</p>
                <p class="mt-1 text-justify">
                    Jl. Kemantren Raya No.5, RT.02/RW.04, Wonosari, Kec. Ngaliyan, Kota Semarang, Jawa
                    Tengah 50186
                </p>
            </div>
            <div class="footer-02">
                <p class="judul-contact text-2xl font-bold">
                    Social Media
                </p>
                <p class="mt-5">secondtaardiansyahw@gmail.com</p>
                <p class="mt-1">+62 839-7862-7867</p>
                <p class="mt-1 flex">
                    <i class="fa-brands fa-instagram text-xl mr-3"></i>
                    <i class="fa-brands fa-github text-xl mx-3"></i>
                    <i class="fa-brands fa-gitlab text-xl mx-3"></i>
                </p>
            </div>
            <div class="footer-03">
                <form action="<?php echo base_url('user/send_email') ?>" method="post">
                    <p class="judul-contact text-2xl font-bold">
                        Get Template
                    </p>
                    <p class="mt-5">
                        <input
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            name="email" id="email" type="email" placeholder="Enter Your Email Here!">
                        <button type="submit"
                            class="mt-3 inline-flex items-center justify-center h-9 px-6 font-medium tracking-wide text-white transition duration-200 bg-gray-900 rounded-lg hover:bg-gray-800 focus:shadow-outline focus:outline-none">
                            Send
                        </button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <script>
    window.onscroll = function() {
        showNavbarOnScroll();
    };

    function showNavbarOnScroll() {
        var navbar = document.querySelector('.navbar');
        var iconProfile = document.querySelector('.iconProfile');
        if (document.body.scrollTop > window.innerHeight || document.documentElement.scrollTop > window.innerHeight) {
            navbar.style.backgroundColor = '#1e1e1e';
            iconProfile.style.color = 'white';
        } else {
            navbar.style.backgroundColor = 'transparent';
            iconProfile.style.color = '#1e1e1e';
        }
    }
    </script>
</body>

</html>