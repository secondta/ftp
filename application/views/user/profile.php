<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/1704420625245-thumbnail.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
    <style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .navbar {
        top: 0;
        width: 100%;
        background-color: #333;
        padding: 15px;
        z-index: 1000;
    }

    .navbar a {
        color: white;
        margin: 0 10px;
        text-decoration: none;
    }

    .navbar-profile button {
        background: none;
        border: none;
        color: white;
        cursor: pointer;
    }

    .navbar-profile {
        display: flex;
        align-items: center;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
        right: 0;
        border-radius: 15px 0px 15px 0px;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        border-radius: 15px 0px 15px 0px;
        margin: 0;
    }

    .dropdown-content a:hover {
        background-color: #f1f1f1;
        border-radius: 15px 0px 15px 0px;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }
    </style>
</head>

<body class="xl:overflow-hidden">
    <div class="navbar w-full top-0 bg-[#1e1e1e] text-white px-4 py-1">
        <div class="flex justify-between">
            <a href="<?php echo base_url();?>" class="w-[50px] h-[50px]">
                <img src="<?php echo base_url('uploads/logo/1704420625245-thumbnail-removebg-preview.png');?>"
                    alt="Logo" width="50px">
            </a>
            <?php if ( $this->session->userdata('logged_in') === true ) : ?>
            <div class="navbar-profile">
                <div class="dropdown">
                    <button id="profile-dropdown" class="profile-button">
                        <i class="far fa-user text-xl"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo base_url('user/profile');?>">Profile</a>
                        <a href="<?php echo base_url('auth/logout');?>">Logout</a>
                    </div>
                </div>
            </div>
            <?php else : ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="all font-web bg-slate-100">
        <div class="min-h-screen flex justify-center items-center">
            <div class="w-3/4 rounded-xl bg-white p-4 xl:-mt-[58px] container">
                <div class="grid grid-cols-6 gap-10">
                    <div class="shadow-2xl rounded-xl col-span-2 mx-auto p-6">
                        <form action="<?php echo base_url('user/aksi_ubah_foto') ?>" enctype="multipart/form-data" method="post">
                            <img src="<?php echo base_url('uploads/user/' . $user['picture']);?>" alt="profile picture" class="rounded-full"
                                width="100%" />
                            <input type="file" name="foto" id="foto" requere
                                class=" mt-5 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                            <button type="submit"
                                class="w-full mt-5 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                                Change
                            </button>
                            <p class="mt-5 text-sm text-justify">
                                File size: maximum 10,000,000 bytes (10 Megabytes). Allowed file extensions: .JPG .JPEG
                                .PNG
                            </p>
                        </form>
                    </div>
                    <div class="py-3 w-full col-span-4">
                        <p class="text-gray-600 font-bold">Personal data</p>
                        <div class="grid grid-cols-4 mt-7">
                            <div class="col-span-1">Username</div>
                            <div class="truncate col-span-3"><?php echo $user['username'] . $user['tag'] ?></div>
                        </div>
                        <div class="grid grid-cols-4 mt-5">
                            <div class="col-span-1">Name</div>
                            <div class="truncate col-span-3"><?php echo $user['name'] ?></div>
                        </div>
                        <div class="grid grid-cols-4 mt-5">
                            <div class="col-span-1">Gender</div>
                            <div class="truncate col-span-3"><?php echo $user['gender'] ?></div>
                        </div>
                        <div class="grid grid-cols-4 mt-5">
                            <div class="col-span-1">Portfolio</div>
                            <div class="truncate col-span-3"><a href="<?php echo base_url();?>"
                                    class="text-cyan-500">Create Portfolio</a></div>
                        </div>
                        <div class="grid grid-cols-4 mt-5">
                            <div class="col-span-1">Password</div>
                            <div class="truncate col-span-3"><a href="<?php echo base_url('user/edit_password');?>"
                                    class="text-cyan-500">Change Password</a></div>
                        </div>
                        <p class="text-gray-600 font-bold mt-7">Contact</p>
                        <div class="grid grid-cols-4 mt-7">
                            <div class="col-span-1">Email</div>
                            <div class="truncate col-span-3"><?php echo $user['email'] ?></div>
                        </div>
                        <div class="grid grid-cols-4 mt-7">
                            <div class="col-span-1">Phone</div>
                            <div class="truncate col-span-3"><?php echo $user['phone'] ?></div>
                        </div>
                        <a href="<?php echo base_url('user/edit_profile');?>"
                            class="w-full cursor-pointer mt-5 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                            Edit Profile
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>