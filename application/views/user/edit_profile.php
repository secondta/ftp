<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/1704420625245-thumbnail.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
    <style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .navbar {
        top: 0;
        width: 100%;
        background-color: #333;
        padding: 15px;
        z-index: 1000;
    }

    .navbar a {
        color: white;
        margin: 0 10px;
        text-decoration: none;
    }

    .navbar-profile button {
        background: none;
        border: none;
        color: white;
        cursor: pointer;
    }

    .navbar-profile {
        display: flex;
        align-items: center;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
        right: 0;
        border-radius: 15px 0px 15px 0px;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        border-radius: 15px 0px 15px 0px;
        margin: 0;
    }

    .dropdown-content a:hover {
        background-color: #f1f1f1;
        border-radius: 15px 0px 15px 0px;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }
    </style>
</head>

<body class="xl:overflow-hidden">
    <div class="navbar w-full top-0 bg-[#1e1e1e] text-white px-4 py-1">
        <div class="flex justify-between">
            <a href="<?php echo base_url();?>" class="w-[50px] h-[50px]">
                <img src="<?php echo base_url('uploads/logo/1704420625245-thumbnail-removebg-preview.png');?>"
                    alt="Logo" width="50px">
            </a>
            <?php if ( $this->session->userdata('logged_in') === true ) : ?>
            <div class="navbar-profile">
                <div class="dropdown">
                    <button id="profile-dropdown" class="profile-button">
                        <i class="far fa-user text-xl"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo base_url('user/profile');?>">Profile</a>
                        <a href="<?php echo base_url('auth/logout');?>">Logout</a>
                    </div>
                </div>
            </div>
            <?php else : ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="all font-web bg-slate-100">
        <div class="min-h-screen flex justify-center items-center">
            <div class="w-3/4 rounded-xl bg-white p-4 xl:-mt-[58px] container">
                <div class="flex justify-between items-center mb-3 px-5">
                    <p class="text-gray-600 font-bold text-2xl">Personal data</p>
                    <a href="javascript:window.history.go(-1);"
                        class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                        <i class="fa-solid fa-arrow-left text-xl mr-3"></i> Back
                    </a>
                </div>
                <hr>
                <form action="<?php echo base_url('user/aksi_ubah_profil') ?>" method="post">
                    <div class="py-3 w-full grid grid-cols-2 gap-5">
                        <div class="mt-2">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                                Email
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value="<?php echo $user['email'] ?>" name="email" id="email" type="text"
                                placeholder="Email">
                        </div>
                        <div class="mt-2">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                Username
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value="<?php echo $user['username'] ?>" name="username" id="username" type="text"
                                placeholder="Username">
                        </div>
                        <div class="mt-2">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                                Name
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value="<?php echo $user['name'] ?>" name="name" id="name" type="text"
                                placeholder="Name">
                        </div>
                        <div class="mt-2">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="phone">
                                Phone
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value="<?php echo $user['phone'] ?>" name="phone" id="phone" type="text"
                                placeholder="Phone">
                        </div>
                        <div class="col-span-2 mt-2">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="gender">
                                Gender
                            </label>
                            <div class="relative">
                                <select
                                    class="shadow cursor-pointer appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="gender" name="gender">
                                    <?php if ( $user['gender'] === 'Male' ) : ?>
                                    <option value="Male" selected>Male</option>
                                    <option value="Female">Female</option>
                                    <?php else : ?>
                                    <option value="Male">Male</option>
                                    <option value="Female" selected>Female</option>
                                    <?php endif; ?>
                                </select>
                                <div
                                    class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 20">
                                        <path
                                            d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <button type="submit"
                            class="col-span-2 w-full cursor-pointer mt-3 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                            Edit Profile
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>