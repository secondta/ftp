<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
</head>

<body>
    <div class="all font-web">
        <div class="flex">
            <?php $this->load->view('admin/components/sidebar') ?>
            <div class="content-page-dashboard max-h-screen container p-8 min-h-screen">
                <div class="header flex justify-between items-center">
                    <h1 class="font-bold text-2xl">Add Template</h1>
                    <nav aria-label="Breadcrumb">
                        <ol class="flex items-center gap-1 text-sm text-gray-600" style="font-size: 17px;">
                            <li>
                                <a href="<?php echo base_url('admin');?>" class="block transition hover:text-gray-700">
                                    <span class="sr-only"> Home </span>

                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                </a>
                            </li>

                            <li class="rtl:rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </li>

                            <li>
                                <a href="<?php echo base_url('admin/template');?>" class="block transition hover:text-gray-700"> Template </a>
                            </li>
                            
                            <li class="rtl:rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </li>

                            <li>
                                <a href="#" class="block transition hover:text-gray-700"> Add Template </a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="content mt-5">
                    <div class="grid grid-cols-2 gap-4">
                        <div class="col-span-2 p-3 rounded-xl shadow-lg">
                            <p class="text-lg font-bold border-b-2 py-2">
                            Add Template
                            </p>
                            <form action="<?php echo base_url('admin/action_add_template') ?>" enctype="multipart/form-data" method="post">
                                <div class="py-3 w-full grid grid-cols-2 gap-5">
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="link">
                                            Link
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            name="link" id="link" type="text" placeholder="Link">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="title">
                                            Title
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            name="title" id="title" type="text" placeholder="Title">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="description">
                                            Description
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            name="description" id="description" type="text" placeholder="Description">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="tag">
                                            Tag
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            name="tag" id="tag" type="text" placeholder="Tag">
                                    </div>
                                    <div class="mt-2 col-span-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="thumbnail">
                                            Thumbnail
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            name="foto" id="thumbnail" type="file">
                                    </div>
                                </div>
                                <button type="submit"
                                    class="col-span-2 w-full cursor-pointer mt-3 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                                    Add Template
                                </button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>