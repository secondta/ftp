<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
    <style>
    .content-page {
        -ms-overflow-style: none;
        /* Internet Explorer 10+ */
        scrollbar-width: none;
        /* Firefox */
    }

    .content-page::-webkit-scrollbar {
        display: none;
        /* Safari and Chrome */
    }
    </style>
</head>

<body>
    <div class="all font-web">
        <div class="flex">
            <?php $this->load->view('admin/components/sidebar') ?>
            <div class="content-page-dashboard max-h-screen container p-8 min-h-screen">
                <div class="header flex justify-between items-center">
                    <h1 class="font-bold text-2xl">Detail User</h1>
                    <nav aria-label="Breadcrumb">
                        <ol class="flex items-center gap-1 text-sm text-gray-600" style="font-size: 17px;">
                            <li>
                                <a href="<?php echo base_url('admin');?>" class="block transition hover:text-gray-700">
                                    <span class="sr-only"> Home </span>

                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                </a>
                            </li>

                            <li class="rtl:rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </li>

                            <li>
                                <a href="<?php echo base_url('admin/user');?>" class="block transition hover:text-gray-700"> User </a>
                            </li>
                            <li class="rtl:rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </li>

                            <li>
                                <a href="" class="block transition hover:text-gray-700"> Detail User </a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="content mt-5">
                    <div class="w-full rounded-xl shadow-xl p-4 container">
                        <div class="grid grid-cols-6 gap-10">
                            <div class="col-span-2 mx-auto p-6 flex items-center">
                                <img src="<?php echo base_url('uploads/user/' . $user['picture']);?>"
                                    alt="profile picture" class="rounded-full" width="100%" />
                            </div>
                            <div class="py-3 w-full col-span-4 ">
                                <p class="text-gray-600 font-bold">Personal data</p>
                                <div class="grid grid-cols-4 mt-7">
                                    <div class="col-span-1">Username</div>
                                    <div class="truncate col-span-3"><?php echo $user['username'] . $user['tag'] ?>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 mt-5">
                                    <div class="col-span-1">Name</div>
                                    <div class="truncate col-span-3"><?php echo $user['name'] ?></div>
                                </div>
                                <div class="grid grid-cols-4 mt-5">
                                    <div class="col-span-1">Gender</div>
                                    <div class="truncate col-span-3"><?php echo $user['gender'] ?></div>
                                </div>
                                <div class="grid grid-cols-4 mt-5">
                                    <div class="col-span-1">Portfolio</div>
                                    <div class="truncate col-span-3"><a href="<?php echo base_url();?>"
                                            class="text-cyan-500">Link Portfolio</a></div>
                                </div>
                                <p class="text-gray-600 font-bold mt-7">Contact</p>
                                <div class="grid grid-cols-4 mt-7">
                                    <div class="col-span-1">Email</div>
                                    <div class="truncate col-span-3"><?php echo $user['email'] ?></div>
                                </div>
                                <div class="grid grid-cols-4 mt-7">
                                    <div class="col-span-1">Phone</div>
                                    <div class="truncate col-span-3"><?php echo $user['phone'] ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!--Datatables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script>
    $(document).ready(function() {

        var table = $('#example').DataTable({
                responsive: true
            })
            .columns.adjust()
            .responsive.recalc();
    });
    </script>
</body>

</html>