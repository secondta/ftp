<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
</head>

<body>
    <div class="all font-web">
        <div class="flex">
            <?php $this->load->view('admin/components/sidebar') ?>
            <div class="content-page-dashboard max-h-screen container p-8 min-h-screen">
                <div class="header flex justify-between items-center">
                    <h1 class="font-bold text-2xl">Setting</h1>
                    <nav aria-label="Breadcrumb">
                        <ol class="flex items-center gap-1 text-sm text-gray-600" style="font-size: 17px;">
                            <li>
                                <a href="<?php echo base_url('admin');?>" class="block transition hover:text-gray-700">
                                    <span class="sr-only"> Home </span>

                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                </a>
                            </li>

                            <li class="rtl:rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </li>

                            <li>
                                <a href="#" class="block transition hover:text-gray-700"> Setting </a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="content mt-5">
                    <div class="grid grid-cols-3 gap-4">
                        <div class="col-span-1 p-3 rounded-xl shadow-lg">
                            <form action="<?php echo base_url('admin/aksi_ubah_foto') ?>" enctype="multipart/form-data"
                                method="post">
                                <input type="file" name="foto" id="foto" class="hidden">
                                <p class="text-lg font-bold border-b-2 py-2">
                                    Change Photo Profile
                                </p>
                                <div class="flex justify-center border-b-2 py-2">
                                    <img src="<?php echo base_url('uploads/user/' . $user['picture']);?>"
                                        alt="Photo Profile" class="rounded-full" width="140">
                                </div>
                                <p class="text-lg font-bold py-2 text-center">
                                    Preview Image:
                                </p>
                                <div class="flex justify-center">
                                    <img class="rounded-full" id="preview-image" width="140">
                                </div>
                                <div class="flex justify-center">
                                    <label for="foto"
                                        class="h-10 px-5 m-1 cursor-pointer flex items-center justify-center text-blue-100 transition-colors duration-150 bg-blue-600 rounded-lg focus:shadow-outline hover:bg-blue-700">Edit
                                        Photo</label>
                                    <button type="submit"
                                        class="h-10 px-5 m-1 text-green-100 transition-colors duration-150 bg-green-700 rounded-lg focus:shadow-outline hover:bg-green-800">Save
                                        Image</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-span-2 p-3 rounded-xl shadow-lg">
                            <p class="text-lg font-bold border-b-2 py-2">
                                Change Personal Data
                            </p>
                            <form action="<?php echo base_url('admin/aksi_ubah_profil') ?>" method="post">
                                <div class="py-3 w-full grid grid-cols-2 gap-5">
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                                            Email
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            value="<?php echo $user['email'] ?>" name="email" id="email" type="text"
                                            placeholder="Email">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                            Username
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            value="<?php echo $user['username'] ?>" name="username" id="username"
                                            type="text" placeholder="Username">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                                            Name
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            value="<?php echo $user['name'] ?>" name="name" id="name" type="text"
                                            placeholder="Name">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="phone">
                                            Phone
                                        </label>
                                        <input
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                            value="<?php echo $user['phone'] ?>" name="phone" id="phone" type="text"
                                            placeholder="Phone">
                                    </div>
                                    <div class="col-span-2 mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="gender">
                                            Gender
                                        </label>
                                        <div class="relative">
                                            <select
                                                class="shadow cursor-pointer appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                id="gender" name="gender">
                                                <?php if ( $user['gender'] === 'Male' ) : ?>
                                                <option value="Male" selected>Male</option>
                                                <option value="Female">Female</option>
                                                <?php else : ?>
                                                <option value="Male">Male</option>
                                                <option value="Female" selected>Female</option>
                                                <?php endif; ?>
                                            </select>
                                            <div
                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 20 20">
                                                    <path
                                                        d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit"
                                        class="col-span-2 w-full cursor-pointer mt-3 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                                        Edit Profile
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-span-3 p-3 rounded-xl shadow-lg mt-3">
                            <p class="text-lg font-bold border-b-2 py-2">
                                Change Password
                            </p>
                            <form action="<?php echo base_url('admin/aksi_ubah_password') ?>" method="post">
                                <div class="py-3 w-full grid grid-cols-2 gap-5">
                                    <div class="mt-2 col-span-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="old_password">
                                            Old Password
                                        </label>
                                        <div class="relative">
                                            <input
                                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                name="password_lama" id="oldPassword" type="password" placeholder="Old Password">
                                            <span
                                                class="absolute right-0 top-0 mt-2 mr-4 cursor-pointer text-gray-600 hover:text-gray-900"
                                                id="toggleOldPassword">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2" for="new_password">
                                            New Password
                                        </label>
                                        <div class="relative">
                                            <input
                                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                name="password_baru" id="newPassword" type="password" placeholder="Old Password">
                                            <span
                                                class="absolute right-0 top-0 mt-2 mr-4 cursor-pointer text-gray-600 hover:text-gray-900"
                                                id="toggleNewPassword">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-gray-700 text-sm font-bold mb-2"
                                            for="confirm_password">
                                            Confirm Password
                                        </label>
                                        <div class="relative">
                                            <input
                                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                name="konfirmasi_password" id="confirmPassword" type="password" placeholder="Old Password">
                                            <span
                                                class="absolute right-0 top-0 mt-2 mr-4 cursor-pointer text-gray-600 hover:text-gray-900"
                                                id="toggleConfirmPassword">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <button type="submit"
                                        class="col-span-2 w-full cursor-pointer mt-3 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                                        Edit Password
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#foto').on('change', function(e) {
            var fileInput = $(this)[0];
            var file = fileInput.files[0];
            var reader = new FileReader();

            if (file) {
                reader.onload = function(e) {
                    $('#preview-image').attr('src', e.target.result);
                    $('#preview-container').show();
                }
                reader.readAsDataURL(file);
            } else {
                $('#preview-container').hide();
            }
        });
    });

    // Old Password
    const oldPasswordInput = document.getElementById('oldPassword');
    const toggleOldPassword = document.getElementById('toggleOldPassword');

    toggleOldPassword.addEventListener('click', () => {
        const type = oldPasswordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        oldPasswordInput.setAttribute('type', type);
        toggleOldPassword.innerHTML = type === 'password' ? '<i class="fas fa-eye"></i>' :
            '<i class="fas fa-eye-slash"></i>';
    });

    // New Password
    const newPasswordInput = document.getElementById('newPassword');
    const toggleNewPassword = document.getElementById('toggleNewPassword');

    toggleNewPassword.addEventListener('click', () => {
        const type = newPasswordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        newPasswordInput.setAttribute('type', type);
        toggleNewPassword.innerHTML = type === 'password' ? '<i class="fas fa-eye"></i>' :
            '<i class="fas fa-eye-slash"></i>';
    });

    // Confirm Password
    const confirmPasswordInput = document.getElementById('confirmPassword');
    const toggleConfirmPassword = document.getElementById('toggleConfirmPassword');

    toggleConfirmPassword.addEventListener('click', () => {
        const type = confirmPasswordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        confirmPasswordInput.setAttribute('type', type);
        toggleConfirmPassword.innerHTML = type === 'password' ? '<i class="fas fa-eye"></i>' :
            '<i class="fas fa-eye-slash"></i>';
    });
    </script>
</body>

</html>