<div class="sidebar max-h-screen min-h-screen bg-[#1e1e1e] min-w-[20%] text-white flex flex-col">
    <div class="header text-3xl font-bold mx-4 mt-4">
        FTP ADMIN
    </div>
    <hr class="mt-3">
    <ul class="mt-4 mx-5">
        <li
            class="cursor-pointer text-[19px] px-3 pt-2 pb-1 rounded-lg hover:bg-gray-100 hover:text-black <?=$page == 'dashboard' ? 'bg-gray-100 text-black ' :'' ?>">
            <a href="<?php echo base_url('admin/dashboard') ;?>" class="flex gap-5">
                <div>
                    <i class="fa-solid fa-chart-line"></i>
                </div>
                <div>
                    Dashboard
                </div>
            </a>
        </li>
        <li
            class="cursor-pointer text-[19px] px-3 pt-2 pb-1 rounded-lg mt-4 hover:bg-gray-100 hover:text-black <?=$page == 'user' ? 'bg-gray-100 text-black ' :'' ?>">
            <a href="<?php echo base_url('admin/user') ;?>" class="flex gap-5">
                <div>
                    <i class="fa-regular fa-user"></i>
                </div>
                <div>
                    User
                </div>
            </a>
        </li>
        <li
            class="cursor-pointer text-[19px] px-3 pt-2 pb-1 rounded-lg mt-4 hover:bg-gray-100 hover:text-black <?=$page == 'template' ? 'bg-gray-100 text-black ' :'' ?>">
            <a href="<?php echo base_url('admin/template') ;?>" class="flex gap-5">
                <div>
                    <i class="fa-regular fa-address-card"></i>
                </div>
                <div>
                    Template
                </div>
            </a>
        </li>
        <li
            class="cursor-pointer text-[19px] px-3 pt-2 pb-1 rounded-lg mt-4 hover:bg-gray-100 hover:text-black <?=$page == 'setting' ? 'bg-gray-100 text-black ' :'' ?>">
            <a href="<?php echo base_url('admin/setting') ;?>" class="flex gap-5">
                <div>
                    <i class="fa-solid fa-gear"></i>
                </div>
                <div>
                    Setting
                </div>
            </a>
        </li>
    </ul>
    <ul class="mt-auto mb-3 mx-5">
        <li class="cursor-pointer text-[19px] px-3 pt-2 pb-1 rounded-lg mt-4 hover:bg-gray-100 hover:text-black">
            <a href="<?php echo base_url('auth/logout') ;?>" class="flex gap-5">
                <div>
                    <i class="fa-solid fa-arrow-right-from-bracket"></i>
                </div>
                <div>
                    Logout
                </div>
            </a>
        </li>
    </ul>
</div>