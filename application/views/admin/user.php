<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
</head>

<body>
    <div class="all font-web">
        <div class="flex">
            <?php $this->load->view('admin/components/sidebar') ?>
            <div class="content-page-dashboard max-h-screen container p-8 min-h-screen">
                <div class="header flex justify-between items-center">
                    <h1 class="font-bold text-2xl">User</h1>
                    <nav aria-label="Breadcrumb">
                        <ol class="flex items-center gap-1 text-sm text-gray-600" style="font-size: 17px;">
                            <li>
                                <a href="<?php echo base_url('admin');?>" class="block transition hover:text-gray-700">
                                    <span class="sr-only"> Home </span>

                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                </a>
                            </li>

                            <li class="rtl:rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path fill-rule="evenodd"
                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </li>

                            <li>
                                <a href="#" class="block transition hover:text-gray-700"> User </a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="content mt-5">
                    <div class="table-user mt-5 p-3 rounded-xl shadow-lg">
                        <table id="example" class="stripe hover"
                            style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                            <thead>
                                <tr>
                                    <th data-priority="1" class="text-left">No</th>
                                    <th data-priority="2" class="text-left">Name</th>
                                    <th data-priority="3" class="text-left">Username</th>
                                    <th data-priority="4" class="text-left">Email</th>
                                    <th data-priority="5">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0; foreach ($user as $row): $no++; ?>
                                <tr>
                                    <td><?php echo $no ?></td>
                                    <td><?php echo $row->name ?></td>
                                    <td><?php echo $row->username ?></td>
                                    <td><?php echo $row->email ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url('admin/detail_user/' . $row->id_user) ;?>"
                                            class="py-2 px-5 text-blue-100 transition-colors duration-150 bg-blue-600 rounded-lg focus:shadow-outline hover:bg-blue-700">
                                            <i class="fa-regular fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!--Datatables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script>
    $(document).ready(function() {

        var table = $('#example').DataTable({
                responsive: true
            })
            .columns.adjust()
            .responsive.recalc();
    });
    </script>
</body>

</html>