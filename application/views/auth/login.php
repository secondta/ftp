<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/jpeg" href="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" />
    <title>FTP</title>
    <?php $this->load->view('style/css') ?>
    <style>
    .no-scrollbar::-webkit-scrollbar {
        display: none;
    }

    .no-scrollbar {
        -ms-overflow-style: none;
        scrollbar-width: none;
    }
    </style>
</head>

<body class="no-scrollbar">
    <div class="all font-web bg-slate-100 min-h-screen flex justify-center items-center">
        <div class="bg-white w-3/4 rounded-xl">
            <div class="grid grid-cols-2">
                <div class="my-auto">
                    <form action="<?php echo base_url('auth/aksi_login') ?>" method="post">
                        <div class="px-14">
                            <p class="font-semibold text-2xl">Login</p>
                            <div class="mt-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                                    Email
                                </label>
                                <input
                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    name="email" id="email" type="text" placeholder="Email">
                            </div>
                            <div class="mt-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                                    Password
                                </label>
                                <div class="relative">
                                    <input
                                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                        name="password" id="password" type="password" placeholder="Password">
                                    <span
                                        class="absolute right-0 top-0 mt-2 mr-4 cursor-pointer text-gray-600 hover:text-gray-900"
                                        id="togglePassword">
                                        <i class="fas fa-eye"></i>
                                    </span>
                                </div>
                            </div>
                            <button type="submit"
                                class="w-full cursor-pointer mt-5 inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-gray-600 whitespace-no-wrap bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:shadow-none">
                                Register
                            </button>
                            <p class="mt-4 text-sm">
                                Don't have an account yet?
                                <span class="mt-4 text-cyan-500">
                                    <a href="<?php echo base_url('auth/register');?>">Register now</a>
                                </span>
                            </p>
                        </div>
                    </form>
                </div>
                <div class="flex justify-center py-10 bg-[#1e1e1e] rounded-tr-xl rounded-br-xl">
                    <img src="<?php echo base_url('uploads/logo/logo-with-text.jpeg');?>" alt="Logo" width="400px">
                </div>
            </div>
        </div>
    </div>
    <script>
    const passwordInput = document.getElementById('password');
    const togglePassword = document.getElementById('togglePassword');

    togglePassword.addEventListener('click', () => {
        const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordInput.setAttribute('type', type);
        togglePassword.innerHTML = type === 'password' ? '<i class="fas fa-eye"></i>' :
            '<i class="fas fa-eye-slash"></i>';
    });
    </script>
</body>

</html>