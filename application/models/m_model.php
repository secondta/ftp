<?php 

class m_model extends CI_Model
{
    public function get($table)
    {
        return $this->db->get($table)->result();
    }

    public function get_total($table)
    {
        return $this->db->get($table)->num_rows();
    }

    public function get_by_id($table, $id_col, $id)
    {
        $data = $this->db->where($id_col, $id)->get($table);
        return $data;
    }

    public function getwhere($table, $where)
    {
        $data = $this->db->where($where)->get($table);
        return $data;
    }

    public function add($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $data, $where)
    {
        $data = $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($table, $field, $id_kelas)
    {
        $data = $this->db->delete($table, array($field => $id_kelas));
        return $data;
    }
}

?>