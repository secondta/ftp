<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
		$this->load->library('session');
        // $this->load->helpers('my_helper');
    }

    public function index()
    {
        redirect(base_url('auth/login'));
	}

    public function acak($long)
    {
        $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $string = '';
        for ($i = 0; $i < $long; $i++) {
            $pos = rand(0, strlen($char) - 1);
            $string .= $char[$pos];
        }
        return $string;
    }
    
    // Login
	public function login()
	{
        if ($this->session->userdata('logged_in') === true) {
            redirect(base_url());
        }
		$this->load->view('auth/login');
	}

    public function aksi_login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data = ['email' => $email];
        $query = $this->m_model->getwhere('user', $data);
        $result = $query->row_array();
        if (!empty($result) && md5($password) === $result['password']) {
            $data = [
                'logged_in' => true,
                'email' => $result['email'],
                'username' => $result['username'],
                'role' => $result['role'],
                'id' => $result['id_user'],
            ];
            $this->session->set_userdata($data);
            if ($result['role'] == 'User') {
                redirect(base_url());
            } else {
                redirect(base_url('admin'));
            }
            
        } else {
            $this->session->set_flashdata('error' , 'error ');
            redirect(base_url('auth/login'));
        }
    }

    // Register
	public function register()
	{
        if ($this->session->userdata('logged_in') === true) {
            redirect(base_url());
        }
		$this->load->view('auth/register');
	}

	public function aksi_register()
    {
        $password = md5($this->input->post('password'));
        $data = [
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'tag' => '#' . $this->acak(6),
            'password' => $password,
            'name' => '-',
            'phone' => '-',
            'gender' => '-',
            'role' => 'User',
            'picture' => 'user.jpg',
        ];
        $email = ['email' => $this->input->post('email')];
        $query_email = $this->m_model->getwhere('user', $email);
        $result_email = $query_email->row_array();
        $username = ['username' => $this->input->post('username')];
        $query_username = $this->m_model->getwhere('user', $username);
        $result_username = $query_username->row_array();
		$cek_password = $this->input->post('password');
		if (!empty($result_email)) {
            $this->session->set_flashdata('error_email', 'aasdasdasd');
            redirect(base_url('auth/register'));
		} elseif (strlen($cek_password) < 8) {
            $this->session->set_flashdata('error_password', 'aasdasdasdasdasd');
            redirect(base_url('auth/register'));
		}elseif (!empty($result_username)) {
            $this->session->set_flashdata('error_username', 'aasdasdasdasd');
            redirect(base_url('auth/register'));
		} else {
            $this->m_model->add('user', $data);
            redirect(base_url('auth/login'));
		}
    }

    // Logout
    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}