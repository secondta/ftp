<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
		$this->load->library('upload');
		$this->load->library('session');
    }
	
	public function index()
	{
		$data['template_portfolio'] = $this->m_model->get('template_portfolio');
		$this->load->view('user/home', $data);
	}

	public function acak($long)
    {
        $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $string = '';
        for ($i = 0; $i < $long; $i++) {
            $pos = rand(0, strlen($char) - 1);
            $string .= $char[$pos];
        }
        return '#' + $string;
    }

	public function upload_img($value)
    {
        $kode = round(microtime(true) * 1000);
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '30000';
        $config['file_name'] = $kode;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($value)) {
            return array(false, '');
        } else {
            $fn = $this->upload->data();
            $nama = $fn['file_name'];
            return array(true, $nama);
        }
    }
    
	public function profile()
	{
		$data['user'] = $this->m_model->get_by_id('user', 'id_user', $this->session->userdata('id'))->row_array();
		$this->load->view('user/profile', $data);
	}
    
	public function edit_profile()
	{
		$data['user'] = $this->m_model->get_by_id('user', 'id_user', $this->session->userdata('id'))->row_array();
		$this->load->view('user/edit_profile', $data);
	}

	public function aksi_ubah_profil() {
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$gender = $this->input->post('gender');
	
		$data = array(
			'email' => $email,
			'username' => $username,
			'name' => $name,
			'phone' => $phone,
			'gender' => $gender
		);

		$this->session->set_userdata($data);
		$update_result = $this->m_model->update('user', $data, array('id_user' => $this->session->userdata('id')));
	
		if ($update_result) {
			redirect(base_url('user/profile'));
		} else {
			echo 'error';
			redirect(base_url('user/edit_profile'));
		}
	}

	public function aksi_ubah_foto()
    {
        $foto = $this->upload_img('foto');
        if ($foto[0] == false) {
			redirect(base_url('user/foto_kosong'));
        } else {
            $data = [
                'picture' => $foto[1],
            ];
        }

        $update_result = $this->m_model->update('user', $data, array('id_user' => $this->session->userdata('id')));
	
		if ($update_result) {
			redirect(base_url('user/profile'));
		} else {
			echo 'error';
			redirect(base_url('user/profile'));
		}
    }

	public function send_email() {
		$mail = new PHPMailer(true);

		$email              = $this->input->post('email');

		$mail->isSMTP();
		$mail->Host       = 'smtp.gmail.com';
		$mail->SMTPAuth   = true;
		$mail->Username   = 'secondtaardiansyahw@gmail.com';
		$mail->Password   = 'zrvg xrgj ojxs sxyy';
		$mail->Port       = 587;
	
		//Recipients
		$mail->setFrom('secondtaardiansyahw@gmail.com');
		$mail->addAddress($email); 
	
		$mail->addReplyTo('secondtaardiansyahw@gmail.com');
		$mail->isHTML(true);
		$mail->Subject = 'Pilih dan Masukan Data yang Diperlukan';
		$mail->Body    = '<h1>Halo, '.$email.'.</h1> <p>Silahkan pilih katalok dan masukan data data yang diperlukan untuk mengisi portfolio anda</p>';    
	
		if($mail->send())
		{
			redirect(base_url(''));
		}
		else{
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}

	public function send_email_port01() {
		$mail = new PHPMailer(true);

		$email              = $this->input->post('email');

		$mail->isSMTP();
		$mail->Host       = 'smtp.gmail.com';
		$mail->SMTPAuth   = true;
		$mail->Username   = 'secondtaardiansyahw@gmail.com';
		$mail->Password   = 'zrvg xrgj ojxs sxyy';
		$mail->Port       = 587;
	
		//Recipients
		$mail->setFrom('secondtaardiansyahw@gmail.com');
		$mail->addAddress($email); 
	
		$mail->addReplyTo('secondtaardiansyahw@gmail.com');
		$mail->isHTML(true);
		$mail->Subject = 'Pilih dan Masukan Data yang Diperlukan';
		$mail->Body    = '<h1>Halo, '.$email.'.</h1> <p>Silahkan masukan data data yang diperlukan untuk mengisi portfolio anda</p>';    
	
		if($mail->send())
		{
			redirect(base_url(''));
		}
		else{
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}

}