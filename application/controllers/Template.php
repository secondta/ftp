<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
		$this->load->library('session');
        // $this->load->helpers('my_helper');
    }

	public function index()
	{
        redirect(base_url());
	}

    // template_01
	public function template_01()
	{
		$this->load->view('template_portfolio/template_01');
	}
}