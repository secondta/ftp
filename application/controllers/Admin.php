<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
		$this->load->library('session');
		$this->load->library('upload');
        // $this->load->helpers('my_helper');
        if ($this->session->userdata('role') !== 'Admin') {
            redirect(base_url());
        }
    }

	public function upload_user($value)
    {
        $kode = round(microtime(true) * 1000);
        $config['upload_path'] = './uploads/user/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '30000';
        $config['file_name'] = $kode;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($value)) {
            return array(false, '');
        } else {
            $fn = $this->upload->data();
            $nama = $fn['file_name'];
            return array(true, $nama);
        }
    }

	public function upload_thumbnail($value)
    {
        $kode = round(microtime(true) * 1000);
        $config['upload_path'] = './uploads/template/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '30000';
        $config['file_name'] = $kode;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($value)) {
            return array(false, '');
        } else {
            $fn = $this->upload->data();
            $nama = $fn['file_name'];
            return array(true, $nama);
        }
    }

	public function index()
	{
        redirect(base_url('admin/dashboard', $data));
	}

    // dashboard
	public function dashboard()
	{
		$data = ['page' => 'dashboard'];
		$data['user'] = $this->m_model->get('user');
		$data['template_portfolio'] = $this->m_model->get('template_portfolio');
		$data['total_user'] = $this->m_model->get_total('user');
		$data['total_template'] = $this->m_model->get_total('template_portfolio');
		$this->load->view('admin/dashboard', $data);
	}

    // user
	public function user()
	{
		$data = ['page' => 'user'];
		$data['user'] = $this->m_model->get('user');
		$this->load->view('admin/user', $data);
	}

	// Detail User
	public function detail_user($id_user)
	{
		$data = ['page' => 'detail_user'];
		$data['user'] = $this->m_model->get_by_id('user', 'id_user', $id_user)->row_array();
		$this->load->view('admin/detail_user', $data);
	}

    // Template
	public function template()
	{
		$data = ['page' => 'template'];
		$data['template'] = $this->m_model->get('template_portfolio');
		$this->load->view('admin/template', $data);
	}
	
	public function add_template()
	{
		$data = ['page' => 'template'];
		$this->load->view('admin/add_template', $data);
	}

	public function action_add_template() {
		$link = $this->input->post('link');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$tag = $this->input->post('tag');
        $foto = $this->upload_thumbnail('foto');

        if ($foto[0] == false) {
			redirect(base_url('admin/foto_kosong'));
        } else {
            $data = array(
				'link_template' => $link,
				'title' => $title,
				'description' => $description,
				'tag' => $tag,
				'thumbnail' => $foto[1],
			);
        }

		$result = $this->m_model->add('template_portfolio', $data);
	
		if ($result) {
			redirect(base_url('admin/template'));
		} else {
			echo 'error';
			redirect(base_url('admin/error'));
		}
	}

    // setting
	public function setting()
	{
		$data = ['page' => 'setting'];
		$data['user'] = $this->m_model->get_by_id('user', 'id_user', $this->session->userdata('id'))->row_array();
		// $data['setting'] = $this->m_model->get('setting');
		$this->load->view('admin/setting', $data);
	}

	public function aksi_ubah_foto()
    {
        $foto = $this->upload_user('foto');
        if ($foto[0] == false) {
			redirect(base_url('user/foto_kosong'));
        } else {
            $data = [
                'picture' => $foto[1],
            ];
        }

        $update_result = $this->m_model->update('user', $data, array('id_user' => $this->session->userdata('id')));
	
		if ($update_result) {
			redirect(base_url('admin/setting'));
		} else {
			echo 'error';
			redirect(base_url('admin/setting'));
		}
    }
	
	public function aksi_ubah_profil() {
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$gender = $this->input->post('gender');
	
		$data = array(
			'email' => $email,
			'username' => $username,
			'name' => $name,
			'phone' => $phone,
			'gender' => $gender
		);

		$this->session->set_userdata($data);
		$update_result = $this->m_model->update('user', $data, array('id_user' => $this->session->userdata('id')));
	
		if ($update_result) {
			redirect(base_url('admin/setting'));
		} else {
			echo 'error';
			redirect(base_url('admin/error'));
		}
	}

	public function aksi_ubah_password() {
		$password_baru = $this->input->post('password_baru');
		$konfirmasi_password = $this->input->post('konfirmasi_password');
		$password_lama = trim($this->input->post('password_lama'));
	
		$user_data = $this->m_model->getwhere('user', array('id_user' => $this->session->userdata('id')))->row_array();
	
		var_dump($password_lama, $user_data['password']);
	
		if (strcmp(md5($password_lama), $user_data['password']) !== 0) {
			$error_password_lama = '*Kata sandi lama salah';
			$this->session->set_flashdata('error_password_lama','*Kata sandi lama salah');
			redirect(base_url('user/profile'));
		}
	
		if (!empty($password_baru)) {
			if ($password_baru === $konfirmasi_password) {
				$data['password'] = md5($password_baru);
			} else {
				$this->session->set_flashdata('error_konfirmasi_password','*Kata sandi baru dan konfirmasi kata sandi harus sama');
				redirect(base_url('user/profile'));
			}
		}
	
		$this->session->set_userdata($data);
		$update_result = $this->m_model->update('user', $data, array('id_user' => $this->session->userdata('id')));
	
		if ($update_result) {
			redirect(base_url('admin/profile'));
		} else {
			echo 'error';
			redirect(base_url('admin/profil'));
		}
	}
}